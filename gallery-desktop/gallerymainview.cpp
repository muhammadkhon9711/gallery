#include "gallerymainview.hpp"
#include "ui_gallerymainview.h"

GalleryMainView::GalleryMainView(QWidget *parent)
    : QMainWindow(parent)
      , ui(new Ui::GalleryMainView)
{
  ui->setupUi(this);
}

GalleryMainView::~GalleryMainView()
{
  delete ui;
}


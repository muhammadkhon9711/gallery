#ifndef GALLERYMAINVIEW_HPP
#define GALLERYMAINVIEW_HPP

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class GalleryMainView; }
QT_END_NAMESPACE

class GalleryMainView : public QMainWindow
{
  Q_OBJECT

public:
  GalleryMainView(QWidget *parent = nullptr);
  ~GalleryMainView();

private:
  Ui::GalleryMainView *ui;
};
#endif // GALLERYMAINVIEW_HPP

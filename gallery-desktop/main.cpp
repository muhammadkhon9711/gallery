#include "gallerymainview.hpp"

#include <QApplication>

int main(int argc, char *argv[])
{
  QApplication a(argc, argv);
  GalleryMainView w;
  w.show();
  return a.exec();
}
